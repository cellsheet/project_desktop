#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <getopt.h>
#include "desktop/desktop.h"
#include "log.h"

static struct desktop desktop;

int main(int argc, char **argv) {
	sway_log_init(SWAY_DEBUG, NULL);

	desktop.display = wl_display_connect(NULL);

	if(!desktop.display) {
		sway_abort("Unable to connect to the compositor. "
	 	            "If your compositor is running, check or set the "
		           "WAYLAND_DISPLAY environment variable.");
	}

	struct wl_registry *registry = wl_display_get_registry(desktop.display);

	sway_log(SWAY_INFO, "Starting desktop client");



	free(registry);
	wl_display_disconnect(desktop.display);

	return 0;
}
