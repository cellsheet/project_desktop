#ifndef DESKTOP_DESKTOP_H
#define DESKTOP_DESKTOP_H

#include <wayland-client.h>

struct desktop {
	struct wl_display *display;
};

#endif //DESKTOP_DESKTOP_H
