# Project Desktop

### Compiling from Source

Install dependencies:

* meson \*
* [wlroots](https://github.com/swaywm/wlroots)
* wayland
* wayland-protocols \*
* pcre
* json-c
* pango
* cairo
* gdk-pixbuf2 (optional: system tray)
* git \*

_\*Compile-time dep_

Run these commands:

    meson build
    ninja -C build
    sudo ninja -C build install

On systems without logind, you need to suid the sway binary:

    sudo chmod a+s /usr/local/bin/sway

Project Desktop will drop root permissions shortly after startup.

## Running

Run project desktop from a TTY. Some display managers may work but are not supported by
project desktop (gdm is known to work fairly well).
